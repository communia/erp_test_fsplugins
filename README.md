# erp_test_fsplugins

Proves del sistema de plugins de fs

Funciona per branques on veiem possibles problemes.

## Branca ModelCollision1
Possibles colisions entre models, de moment no implementat (falta crear Model que
sobreescrigui Asiento en nou mòdul).

## Branca ViewCollision1
Es mostra el problema que si hi ha un  plugin que fa un override d'una vista de 
sistema, com ListAsientos.xml, tindrà preferència la del plugin més recent.
Tenint vista:

| Asiento       |
|---------------|
|Id|
|Name|

Si plugin Nouasiento afegeix nova columna:

|Asiento |
|----|
|Id|
|Name|
|CC|

I posteriorment un altre plugin afegeix nova columna:

|Asiento|
|---|
|Id|
|Name|
|Idsmash|

**No tenim manera possible de fer sortir CC sense incloure-ho a l'últim plugin.**
El mateix passaria amb Formularis d'edició.
